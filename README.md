                                                Vscode C/C++,json文件设置

 
1.配置vscode的tasks.json与launch.json文件,MinGW路径是gbd.exe,例:(C:\MinGW\bin\gdb.exe).


注:QT里面也有gdb.exe,可以配置成QT的.

![](https://github.com/greycatah/vscode_CsetPath/blob/master/photo/image001.png)


 

2.点击C/C++转至定义按钮,进入设置界面.

![](https://github.com/greycatah/vscode_CsetPath/blob/master/photo/image002.png)

![](https://github.com/greycatah/vscode_CsetPath/blob/master/photo/image003.png)

 

点击:添加按钮,添加路径

![](https://github.com/greycatah/vscode_CsetPath/blob/master/photo/image004.png)

 

PATH按钮:默认不生成C_CPP_properties.json文件,

![](https://github.com/greycatah/vscode_CsetPath/blob/master/photo/image005.png)



按下后点击生成就可以生成C_CPP_properties.json文件.

![](https://github.com/greycatah/vscode_CsetPath/blob/master/photo/image006.png)

 
![](https://github.com/greycatah/vscode_CsetPath/blob/master/photo/image007.png)


注:QT的gcc,C++路径跟MinGW有区别.

设置完后点击:确定.

 

3.头文件写入(hello.c,mai.c)以,号隔开

![](https://github.com/greycatah/vscode_CsetPath/blob/master/photo/image008.png)


![](https://github.com/greycatah/vscode_CsetPath/blob/master/photo/image009.png)


![](https://github.com/greycatah/vscode_CsetPath/blob/master/photo/image010.png)

 

4.最后点击:生成.

![](https://github.com/greycatah/vscode_CsetPath/blob/master/photo/image011.png)

 

将生成的3个json文件,拖进vscode工程中的.vscode文件夹中即可编译C/C++语言.

注:没有.vscode文件夹的朋友可以按F5调试,方可配置生成.vscode文件夹.

![](https://github.com/greycatah/vscode_CsetPath/blob/master/photo/image012.png)

